import requests
import json

username = 'your_user'
password = 'your_password'
baseUrl = 'Jira_base_URL'
jiraIssue = "Jira_issue_id"
newDescription = 'new_description'


def changeDescription(issue):
    url = baseUrl + "/rest/api/2/issue/" + issue

    data = {
        "fields":
            {
                # Put # to comment the attributes that you don't want to change.
                #"summary":"Changed summary",
                "description": newDescription
             }
    }

    headers = {
        'Content-type': 'application/json',
        'Accept': 'application/json'
    }

    return requests.put(url=url, data=json.dumps(data), headers=headers, auth=(username, password))


print(changeDescription(jiraIssue).content)